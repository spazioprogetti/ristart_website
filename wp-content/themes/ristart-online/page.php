<?php get_header(); ?>
<main id="primary" class="site-main">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	// Page thumbnail and title.
	the_post_thumbnail();
	the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
	?>
	<div class="entry-content">
	<?php the_content(); ?>
	</div><!-- .entry-content -->
	</article><!-- #post-<?php the_ID(); ?> -->
	<?php
// End the loop.
endwhile;
?>
</main><!-- #main -->
<?php
get_sidebar();
get_footer();
