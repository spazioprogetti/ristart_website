var gulp = require('gulp'),                     // base 
    sass = require('gulp-sass'),                // processes scss files
    concat = require('gulp-concat'),            // to merge files
    del = require('del'),                       // deletes old dist files
    connect = require('gulp-connect'),          // creates a local server
    open = require('gulp-open'),                // to open a browser istance
    //autoprfx = require('gulp-autoprefixer'),  // adds vendor-prefixes
    mustache = require('gulp-mustache'),        // needed to work with mustache templates
    //addsrc = require('gulp-add-src'),         // useful to add some css/js in the bundle 
    cleanCSS = require('gulp-clean-css'),       // minifies css files
    uglify   = require('gulp-uglify'),          // minifies js files
    gulpif   = require('gulp-if'),              // conditions in pipe
    file     = require('gulp-file');            // creates a new file
    //inject   = require('gulp-inject-partials'); // injects a text in a file (used for ATF injection)
    

var debug = true;

var buildFolder = 'dist/',
    cssBuildFolder = buildFolder + 'styles/',
    scriptsBuildFolder = buildFolder + 'scripts/',
    assetsBuildFolder = buildFolder + 'assets/';
    dataBuildFolder = buildFolder + 'data/';


/*gulp.task('atf-files', function() {
    var filename = 'atf-home.php';
    var string = " <style><!-- partial:styles/abovethefold-home.css --><!-- partial --></style>";
    
    return file(filename, string, { src: true })
           .pipe(gulp.dest(buildFolder));
});*/



// task to delete each file in the dist directory
gulp.task('clean', function () {
    return del([cssBuildFolder, scriptsBuildFolder, assetsBuildFolder, dataBuildFolder, buildFolder + '*.html']);
});

// task to compile SASS scripts, 
// create the css files and reload the page
gulp.task('sass', function () {
    return gulp.src('src/styles/**/*.scss')
        .pipe(sass({
            includePaths: ['node_modules'],
            outputStyle: 'compact',
            precision: 9
        }).on('error', sass.logError))
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
         }))
        .pipe(gulp.dest(cssBuildFolder))        
        .pipe(connect.reload());
});


gulp.task('styles:lib', function() {
    return gulp.src('src/styles/lib/*.css')
        .pipe(concat('main-libs.css'))
        .pipe(gulp.dest(cssBuildFolder))
        .pipe(connect.reload());
});



gulp.task('prod', function() { debug = false; return gulp.src('src/');}) // does nothing but set the debug variable

gulp.task('scripts', function() {
    return gulp.src('src/scripts/*.js')
        .pipe(concat('main.js'))
        .pipe(gulpif(!debug, uglify({ 
                                    compress : { // https://stackoverflow.com/questions/24591854/using-gulp-to-concatenate-and-uglify-files, http://lisperator.net/uglifyjs/compress, http://davidwalsh.name/compress-uglify, 
                                                 drop_console : true,
                                               }
                                })
        ))
        .pipe(gulp.dest(scriptsBuildFolder))
        .pipe(connect.reload());
});

gulp.task('scripts:lib', function() {
    return gulp.src('src/scripts/lib/*.js')
        .pipe(concat('main-libs.js'))
        .pipe(gulp.dest(scriptsBuildFolder))
        .pipe(connect.reload());
});


// gulp.task('html', function() {
//    return gulp.src('src/**/*.html')
//        .pipe(gulp.dest(buildFolder))
//        .pipe(connect.reload());
//});

gulp.task('assets', function() {
    return gulp.src('src/assets/**/*')
        .pipe(gulp.dest(assetsBuildFolder));
});

gulp.task('data', function() {
    return gulp.src('src/data/**/*')
        .pipe(gulp.dest(assetsBuildFolder));
});


gulp.task('templates', function() {
    return gulp.src("src/templates/*.mustache")
    .pipe(mustache('mustache.json',{extension: '.html'}))
    .pipe(gulp.dest(buildFolder))
    .pipe(connect.reload());
});



gulp.task('watch', function () {
    gulp.watch('src/styles/**/*.scss', gulp.series('sass'));
    gulp.watch('src/assets/**/*', gulp.series('assets'));
    gulp.watch('src/scripts/*.js', gulp.series('scripts'));
    gulp.watch('src/data/**/*', gulp.series('data'));
    gulp.watch('src/templates/**/*.mustache', gulp.series('templates'));
    //gulp.watch('src/**/*.html', gulp.series('html')); // alternative to run templates
});


gulp.task('connect', function() {
    connect.server({
        root: buildFolder,
        port: 9000,
        livereload: true
    });
});

gulp.task('browser', function(){
    gulp.src(buildFolder + 'index.html')
        .pipe(open({uri: 'http://localhost:9000'}));
});


gulp.task('common-chain', 
    gulp.series('clean', 
        gulp.parallel('assets','sass','scripts','data','scripts:lib','styles:lib','templates' /*, 'html'*/)
                
    )
);

gulp.task('default',
    gulp.series('common-chain', gulp.parallel('connect','watch','browser'))
);

gulp.task('build', gulp.series('common-chain'));

gulp.task('build-prod',  gulp.series('prod','common-chain'));
                
    



