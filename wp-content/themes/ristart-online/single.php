<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ristart.online
 */

get_header();
?>

	<main id="primary" class="site-main">

		
	<?php
        // Start the loop.
        while ( have_posts() ) : the_post();
  
            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
         ?>
		 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Page thumbnail and title.
		the_post_thumbnail();
		the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
	?>
	<div class="entry-content">
				<?php the_content();?>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
		 <?
        // End the loop.
        endwhile;
        ?>
  
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
