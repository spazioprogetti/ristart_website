<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ristart.online
 */

get_header();
?>
<div class="wrapper">
      <div class="main-container">
<section class="hero">
    <div class="grid grid-reverse">
        <div class="col col-6 col-sm">
        <div class="claim">
            <h1>Riapri il tuo ristorante in modo facile e sicuro</h1>
            <p>Riparti in sicurezza con il menu del tuo ristorante online, su ogni tavolo, direttamente sullo smartphone dei tuoi clienti</p>
            <a href="javascript:void(0);" id="cta_1" class="cta-big">Crea il tuo menu gratis</a>
        </div>
        </div>
        <div class="col col-6 col-sm col-first">
        <div class="heroimage">
            <img class="img-responsive" src="/wp-content/themes/ristart-online/dist/assets/hero.svg" alt="ristartOnline">
        </div>
        </div>
    </div>
</section><section class="app">
    <div class="grid">
        <div class="col col-6 col-sm">
            <div class="appimage">
                <img class="img-responsive" src="/wp-content/themes/ristart-online/dist/assets/app.svg" alt="ristartOnline">
            </div>
        </div>
        <div class="col col-6 col-sm">
            <div class="howto">
                <h3>il tuo menù online, su tutti i tavoli</h3>
                <h1>Perchè è sicuro</h1>
                <p>Registrati gratis e crea il tuo menù, scarica il codice QR, voilà. <br>
                    Ai tuoi clienti basterà inquadrare il codice con lo smartphone per leggere subito il tuo menù e ordinare in tutta sicurezza, ognuno con il suo device, <br>
                    nel rispetto delle norme e del distanziamento sociale. <br><br>
                    
                    Puoi stampare il codice dove vuoi, su tavoli, tovaglie, all'ingresso oppure condividerlo sui social.
                    Un solo gesto per rendere il tuo menù disponibile in sicurezza e sempre aggiornato</p>
                <a href="javascript:void(0);" id="cta_2" class="cta-big">Crea subito il tuo menu</a>
            </div>
        </div>
    </div>
</section>

<section class="cards" id="comefunziona">
    <div class="centerclaim">
        <h3>semplice per te, sicuro per i tuo clienti</h3>
        <h1>Come funziona</h1>
    </div>
    <div class="grid">
        <div class="col col-4 col-sm">
            <div class="card">
                <div class="cardicon">
                    <img class="img-responsive" src="/wp-content/themes/ristart-online/dist/assets/join.svg" alt="registra il tuo ristorante su ristartonline">
                </div>
                <h2>Registrati e crea il tuo menu</h2>
                <p>Registra gratis il tuo ristorante e crea il tuo menù con il nostro sistema in 5 minuti</p>
            </div>
        </div>
        <div class="col col-4 col-sm">
            <div class="card">
                <div class="cardicon">
                    <img class="img-responsive" src="/wp-content/themes/ristart-online/dist/assets/qricon.svg" alt="registra il tuo ristorante su ristartonline">
                </div>
                <h2>stampa il codice QR del menu</h2>
                <p>Scarica e stampa il codice QR dove vuoi, il QR sarà sempre lo stesso anche se poi aggiorni il menù</p>
            </div>
        </div>
        <div class="col col-4 col-sm">
            <div class="card">
                <div class="cardicon">
                    <img class="img-responsive" src="/wp-content/themes/ristart-online/dist/assets/mobile.svg" alt="registra il tuo ristorante su ristartonline">
                </div>
                <h2>Proponi il tuo menu online</h2>
                <p>Chiedi ai clienti di inquadrare il codice QR con il proprio smartphone. Facile, veloce e sicuro no?</p>
            </div>
        </div>
    </div>
    <div class="cardscta">
        <a href="javascript:void(0);" id="cta_3" class="cta-big">Inizia subito</a>
    </div>
</section>
      </div>

    </div>

<?php
get_sidebar();
get_footer();
