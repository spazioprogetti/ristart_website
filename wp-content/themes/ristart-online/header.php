<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ristart.online
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="robots" content="noindex">
      <title>Ristart Online</title>
      <meta name="description" content="Riapri il tuo ristorante in sicurezza con il menù online direttamente sullo smartphone dei tuoi clienti.">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" type="text/css" href="/wp-content/themes/ristart-online/dist/styles/main.css">
	  <script src="https://kit.fontawesome.com/0adca9bc1e.js" crossorigin="anonymous"></script>
	  <?php wp_head(); ?>
  </head>


<body <?php body_class(); ?>>

<div class="shape">
    <img src="/wp-content/themes/ristart-online/dist/assets/shape.svg" alt="">
</div>
<header>
    <div class="navbar">
    <div class="navbar-container">
        <div class="logo">
        <img class="logoimage" src="/wp-content/themes/ristart-online/dist/assets/ristart.svg" alt="RistartOnline">
        </div>
        <div class="menu">
            <ul class="desktop">
                <li><a href="#comefunziona">Come funziona</a></li>
                <li><a href="#">accedi</a></li>
                <li><a class="menucta" href="#">registrati</a></li>
            </ul>
            <ul class="mobile">
                <li><a class="menucta" href="#">accedi</a></li>
            </ul>
        </div>
    </div>
    </div>
</header>