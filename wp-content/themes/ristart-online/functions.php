<?php
/**
 * ristart.online functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ristart.online
 */



// custom print_r function
function pre($text, $stop = false)
{
    echo "<pre>";
    print_r($text);
    echo "</pre>";
    if ($stop) {
        exit;
    }
}


/*------------------------------------*\
    MENU Functions
\*------------------------------------*/

function register_my_menus() {
register_nav_menus(
    array(
        'menu-top' => __( 'Menu di Navigazione' ),
        'menu-footer-1' => __( 'Menu nel footer 1' ),
        'menu-footer-2' => __( 'Menu nel footer 2' ),
        'menu-footer-3' => __( 'Menu nel footer 3' ),
        'menu-footer-4' => __( 'Menu nel footer 4' )
        )
        );
    }
add_action( 'init', 'register_my_menus' );



         
// add the filter 
add_filter( 'wp_nav_menu', 'wp_nav_menu_remove_attributes' );
function wp_nav_menu_remove_attributes( $menu ){
    return $menu = preg_replace('/ id=\"(.*)\" class=\"(.*)\"/iU', '', $menu );
}


add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
function clear_nav_menu_item_id($id, $item, $args) {
    return "";
}

add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3);
function clear_nav_menu_item_class($classes, $item, $args) {
    if (in_array("ctabtn",$classes))
        return array("ctabtn");
    else
        return array();
    
    
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

/*------------------------------------*\
    Sidebar Functions
\*------------------------------------*/

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('FOOTER 1'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 2'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 3'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-3',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 4'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-4',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
}

