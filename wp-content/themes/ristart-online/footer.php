<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ristart.online
 */

?>

<footer>
    <div class="footershape">
        <img src="/wp-content/themes/ristart-online/dist/assets/footer.svg" alt="">
    </div>
    <div class="footershape-mobile">
        <img src="/wp-content/themes/ristart-online/dist/assets/footer-mobile.svg" alt="">
    </div>
    <div class="footer-container">
        <div class="logo-footer">
            <img src="/wp-content/themes/ristart-online/dist/assets/ristart_white.svg" alt="ristartonline">
        </div>
        <div class="footer-info">
        <p><a href="mailto:info@ristart.online"> info@ristart.online</a></p>
        </div>
    </div>
</footer>    <div class="modal">
      <div class="modal_shadow"></div>
      <div class="modal_container">
        <a href="javascript:void(0);" class="modal_close"><i class="fas fa-times"></i></a>

        <div class="modal_title">
          <div class="h2">
            form registrazione / login
          </div>
          <h1><div class="modal_icon"><i class="fas fa-user"></i></div> accedi</h1>
        </div>
      </div>
    </div>


    <script src="/wp-content/themes/ristart-online/dist/scripts/main-libs.js"></script>
    <script src="/wp-content/themes/ristart-online/dist/scripts/main.js"></script>
	<?php wp_footer(); ?>
  </body>

</html>